//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:

// Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
// 1.)
// Create a student class sectioning system based on their entrance exam score.
// If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
// If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
// If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
// If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

// Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)

// code here

console.log('Hello World')
console.log(' ')

// part 1
// 1.

function studentSection(studentAverage) {
	if (studentAverage <= 80) {
		console.log('Your score is ' + studentAverage + '. You will proceed to Grade 10, Section Ruby.')
	} else if (studentAverage <= 120 ){
		console.log('Your score is ' + studentAverage + '. You will proceed to Grade 10, Section Opal.')
	} else if (studentAverage <= 160 ){
		console.log('Your score is ' + studentAverage + '. You will proceed to Grade 10, Section Sapphire.')
	} else if (studentAverage <= 200 ){
		console.log('Your score is ' + studentAverage + '. You will proceed to Grade 10, Section Diamond.')
	} 
}

studentSection(75)
studentSection(100)
studentSection(150)
studentSection(190)

// 2.) 
// Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

// Sample Data and output:
// Example string: 'Web Development Tutorial'
// Expected Output: 'Development'

// code here

console.log(' ')
function findLongestWord(str)
{
  var motto = str.match(/\w[a-z]{0,}/gi);
  var longestWord = motto[0];

  for(var x = 1 ; x < motto.length ; x++)
  {
    if(longestWord.length < motto[x].length)
    {
    longestWord = motto[x];
    } 
  }
  return longestWord;
}
console.log(findLongestWord('Education is the passport to the future, for tomorrow belongs to those who prepare for it today. -Malcolm X '));


// 3.)
/*Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'

// */

// code here

// 3. 
console.log(' ')
function findFirstNotRepeatedChar(str) {
  var word = str.split('');
  var firstNotRepeatedChar = '';
  var ctr = 0;
 
  for (var x = 0; x < word.length; x++) {
    ctr = 0;
 
    for (var y = 0; y < word.length; y++) 
    {
      if (word[x] === word[y]) {
        ctr+= 1;
      }
    }
 
    if (ctr < 2) {
    	firstNotRepeatedChar = word[x];
      break;
    }
  }
  return firstNotRepeatedChar;
}
console.log(findFirstNotRepeatedChar('memories'));